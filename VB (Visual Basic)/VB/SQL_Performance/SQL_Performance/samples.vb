﻿Public Class samples

    Private sex As String 'sex of sample
    Private age As Integer 'age of sample
    Private time As Integer 'time of sample
    Private meas As List(Of Integer) 'measurements of sample

    Public Sub New(ByVal sx As String, ae As Integer, te As Integer, ms As List(Of Integer))
        sex = sx
        age = ae
        time = te
        meas = ms
    End Sub
    Public ReadOnly Property getSex As String
        Get
            Return sex
        End Get
    End Property

    Public ReadOnly Property getAge As Integer
        Get
            Return age
        End Get
    End Property

    Public ReadOnly Property getTime As Integer
        Get
            Return time
        End Get
    End Property

    Public ReadOnly Property getMeas As List(Of Integer)
        Get
            Return meas
        End Get
    End Property

    Public Function countMetr() As Integer 'function to count the total number of samples for each entry
        Return meas.Count
    End Function

    Public Overrides Function ToString() As String 'ToString method to print each sample if you want to, I made it to check if it reads the text and initialize the samples object correctly
        Dim retmetr As String = String.Empty
        For Each item As Integer In meas
            If String.IsNullOrEmpty(retmetr) Then
                retmetr += item.ToString()
            Else
                retmetr += String.Format(", {0}", item)
            End If
        Next
        Return "SEX: " + sex.ToString() + " AGE: " + age.ToString() + " TIME: " + time.ToString() + " METRICS: " + retmetr.ToString()
    End Function

End Class
