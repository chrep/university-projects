﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO

Public Class Form3

    Private myParent As Form 'parent form
    Private collection As List(Of samples)
    'use them so I can write all the statistics that the user asked for when he clicks export to txt
    Private avgtimem, avgtimefm, avgtimeb As Double
    Private avgmetrm, avgmetrfm, avgmetrb As Double
    Private mintimem, mintimefm, mintimeb As Integer
    Private minmetrm, minmetrfm, minmetrb As Integer
    Private maxtimem, maxtimefm, maxtimeb As Integer
    Private maxmetrm, maxmetrfm, maxmetrb As Integer
    Private stdev, avedev, median, ave As Double



    Public Sub New(parent As Form, sample As List(Of samples)) ' I made this so I can pass the samples from Form2 to Form3
        InitializeComponent()

        myParent = parent
        collection = sample
    End Sub



    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click 'exits app
        Me.Close()
        Application.Exit()
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click 'when it clicks ok
        Dim choice1 As String = ListBox1.SelectedItem
        Dim choice2 As String = ListBox2.SelectedItem

        If (ListBox1.SelectedIndex = -1 Or ListBox2.SelectedIndex = -1) Then
            MsgBox("Please select one item from each list")
        Else
            If choice1 = "Average Time" Then 'AVERAGE TIME
                Dim avg As Double
                If choice2 = "Male" Then 'AVERAGE TIME FOR MALE
                    Dim mcount As Integer = 0
                    Dim mtime As Integer = 0
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            mcount += 1
                            mtime += item.getTime
                        End If
                    Next
                    avg = mtime / mcount
                    MsgBox("Average Time of Males: " + avg.ToString())
                ElseIf choice2 = "Female" Then 'AVERAGE TIME FOR FEMALE
                    Dim wcount As Integer = 0
                    Dim wtime As Integer = 0
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            wcount += 1
                            wtime += item.getTime
                        End If
                    Next
                    avg = wtime / wcount
                    MsgBox("Average Time of Females: " + avg.ToString())
                Else 'AVERAGE TIME FOR BOTH
                    Dim count = 0
                    Dim time As Integer = 0
                    For Each item As samples In collection
                        count += 1
                        time += item.getTime
                    Next
                    avg = time / count
                    MsgBox("Average Time of Both: " + avg.ToString())
                End If

            ElseIf choice1 = "Average Metrics" Then 'AVERAGE METRICS
                Dim mcount As Integer = 0
                Dim mmetr As Integer = 0
                Dim sam As List(Of Integer)
                Dim avg As Double
                If choice2 = "Male" Then 'AVERAGE METRICS FOR MALES
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                mcount += 1
                                mmetr += j
                            Next
                        End If
                    Next
                    avg = mmetr / mcount
                    MsgBox("Average Metric of Males: " + avg.ToString() + "%")
                ElseIf choice2 = "Female" Then 'AVERAGE METRICS FOR FEMALES
                    Dim wcount As Integer = 0
                    Dim wmetr As Integer = 0
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                wcount += 1
                                wmetr += j
                            Next
                        End If
                    Next
                    avg = wmetr / wcount
                    MsgBox("Average Metric of Females: " + avg.ToString() + "%")
                ElseIf choice2 = "Both" Then 'AVERAGE METRICS FOR BOTH
                    Dim count = 0
                    Dim metr As Integer = 0
                    For Each item As samples In collection
                        sam = item.getMeas
                        For Each j As Integer In sam
                            count += 1
                            metr += j
                        Next
                    Next
                    avg = metr / count
                    MsgBox("Average Metric of Both: " + avg.ToString())
                End If
            ElseIf choice1 = "Minimum Time" Then 'MIN TIME 
                Dim minTime As Integer = 2000000000 'I chose that number so it would be close to the maximum number of the Integer range
                If choice2 = "Male" Then 'MIN TIME FOR MALE
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            If (item.getTime < minTime) Then
                                minTime = item.getTime
                            End If
                        End If
                    Next
                    MsgBox("Minimum Time of Males: " + minTime.ToString())
                ElseIf choice2 = "Female" Then 'MIN TIME FOR FEMALE
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            If (item.getTime < minTime) Then
                                minTime = item.getTime
                            End If
                        End If
                    Next
                    MsgBox("Minimum Time of Males: " + minTime.ToString())
                ElseIf choice2 = "Both" Then  'MIN TIME FOR BOTH
                    For Each item As samples In collection
                        If (item.getTime < minTime) Then
                            minTime = item.getTime
                        End If
                    Next
                    MsgBox("Minimum Time of Both: " + minTime.ToString())
                End If
            ElseIf choice1 = "Minimum Metric" Then 'MIN METRIC
                Dim sam As List(Of Integer)
                Dim minMetric As Integer = 101 'Because the maximum value it's 100%
                If choice2 = "Male" Then 'MIN METRIC FOR MALES
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                If (j < minMetric) Then
                                    minMetric = j
                                End If
                            Next
                        End If
                    Next
                    MsgBox("Minimum Metric of Males: " + minMetric.ToString())
                ElseIf choice2 = "Female" Then 'MIN METRIC FOR FEMALES
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                If (j < minMetric) Then
                                    minMetric = j
                                End If
                            Next
                        End If
                    Next
                    MsgBox("Minimum Metric of Females: " + minMetric.ToString())
                ElseIf choice2 = "Both" Then  'MIN METRIC FOR BOTH
                    For Each item As samples In collection
                        sam = item.getMeas
                        For Each j As Integer In sam
                            If (j < minMetric) Then
                                minMetric = j
                            End If
                        Next
                    Next
                    MsgBox("Minimum Metric of Both: " + minMetric.ToString())
                End If
            ElseIf choice1 = "Maximum Time" Then 'MAX TIME
                Dim maxTime As Integer = -1
                If choice2 = "Male" Then 'MAX TIME FOR MALES
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            If (item.getTime > maxTime) Then
                                maxTime = item.getTime
                            End If
                        End If
                    Next
                    MsgBox("Maximum Time of Males: " + maxTime.ToString())
                ElseIf choice2 = "Female" Then 'MAX TIME FOR FEMALES
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            If (item.getTime > maxTime) Then
                                maxTime = item.getTime
                            End If
                        End If
                    Next
                    MsgBox("Maximum Time of Females: " + maxTime.ToString())
                ElseIf choice2 = "Both" Then  'MAX TIME FOR BOTH
                    For Each item As samples In collection
                        If (item.getTime > maxTime) Then
                            maxTime = item.getTime
                        End If
                    Next
                    MsgBox("Maximum Time of Females: " + maxTime.ToString())
                End If
            ElseIf choice1 = "Maximum Metric" Then 'MAX METRIC
                Dim sam As List(Of Integer)
                Dim maxMetric As Integer = -1
                If choice2 = "Male" Then 'MAX METRIC FOR MALES
                    For Each item As samples In collection
                        If item.getSex = "M" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                If (j > maxMetric) Then
                                    maxMetric = j
                                End If
                            Next
                        End If
                    Next
                    MsgBox("Maximum Metric of Males: " + maxMetric.ToString())
                ElseIf choice2 = "Female" Then 'MAX METRIC FOR FEMALES
                    For Each item As samples In collection
                        If item.getSex = "F" Then
                            sam = item.getMeas
                            For Each j As Integer In sam
                                If (j > maxMetric) Then
                                    maxMetric = j
                                End If
                            Next
                        End If
                    Next
                    MsgBox("Maximum Metric of Females: " + maxMetric.ToString())
                ElseIf choice2 = "Both" Then  'MAX METRIC FOR BOTH
                    For Each item As samples In collection
                        sam = item.getMeas
                        For Each j As Integer In sam
                            If (j > maxMetric) Then
                                maxMetric = j
                            End If
                        Next
                    Next
                    MsgBox("Maximum Metric of Both: " + maxMetric.ToString())
                End If
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'passing data into a multidimensional array 
        Dim rows As Integer = collection.Count
        Dim columns As Integer = collection(0).countMetr
        Dim dataAr(0 To rows - 1, 0 To columns + 2) As Object
        Dim i As Integer = 0
        For Each item As samples In collection
            dataAr(i, 0) = item.getTime
            dataAr(i, 1) = item.getAge
            dataAr(i, 2) = item.getSex
            Dim sams As List(Of Integer) = item.getMeas
            Dim j As Integer = 0
            For Each metr As Integer In sams
                dataAr(i, j + 3) = metr
                j += 1
            Next
            i += 1
        Next
        'starting an excel app to pass the dataAr
        Dim xlApp As Excel.Application
        Dim xlBook As Excel.Workbook
        Dim xlSheet As Excel.Worksheet

        xlApp = New Excel.Application
        xlBook = xlApp.Workbooks.Add
        xlBook.Sheets.Add.Name = "Values"
        xlBook.Sheets.Add.Name = "Chart"
        xlBook.Worksheets("Values").Activate()
        xlSheet = xlBook.ActiveSheet

        xlSheet.Range("A1").Value = "Time"
        xlSheet.Range("B1").Value = "Age"
        xlSheet.Range("C1").Value = "Sex"
        xlSheet.Range("A1:C1").Font.Bold = True
        xlSheet.Range("A1:C1").Font.Underline = True

        Dim NumRows As Long
        Dim NumCols As Long
        NumRows = UBound(dataAr, 1) - LBound(dataAr, 1) + 1
        NumCols = UBound(dataAr, 2) - LBound(dataAr, 2) + 1
        xlSheet.Range("A2").Resize(NumRows, NumCols).Value = dataAr

        Dim range As Excel.Range
        Dim chart As Excel.ChartObject

        range = xlSheet.Range("D:Z").Cells.SpecialCells(Excel.XlCellType.xlCellTypeConstants)
        xlBook.Worksheets("Chart").Activate()
        xlSheet = xlBook.ActiveSheet
        chart = xlSheet.ChartObjects.Add(0, 100, 1000, 400)
        chart.Chart.SetSourceData(range)
        chart.Chart.ChartType = Excel.XlChartType.xlColumnClustered

        Dim file_name As String = "vbtest.xlsx"
        Dim app_path As String = Directory.GetCurrentDirectory()
        Dim file_path As String = app_path.Replace("\bin\Debug", "\") 'getting back to the directory with the code and file. 
        file_path += file_name 'adding the file name in the path

        xlApp.DisplayAlerts = False
        xlBook.SaveAs(file_path)
        xlBook.Close()
        xlApp.Quit()
        MsgBox("Successfully Excel file saved!")
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim file_name As String = "vbtest.xlsx"
        Dim app_path As String = Directory.GetCurrentDirectory()
        Dim file_path As String = app_path.Replace("\bin\Debug", "\") 'getting back to the directory with the code and file. 
        file_path += file_name 'adding the file name in the path
        If File.Exists(file_path) Then
            Dim xlapp As Excel.Application = New Excel.Application
            Dim xlbook As Excel.Workbook
            Dim xlsheet As Excel.Worksheet
            xlbook = xlapp.Workbooks.Open(file_path)
            xlbook.Worksheets("Values").Activate()
            xlsheet = xlbook.ActiveSheet

            Dim xlrange As Excel.Range
            xlrange = xlsheet.Range("D:Z").Cells.SpecialCells(Excel.XlCellType.xlCellTypeConstants)

            If ListBox3.SelectedIndex = -1 Then
                MsgBox("Please select one statistic!")
            Else
                If ListBox3.SelectedItem = "Standard Deviation" Then
                    If (collection(1).countMetr() * collection.Count) > 30 Then
                        MsgBox("The StDev function in Excel.Interop cannot process over 30 arguments!")
                    Else
                        Dim stdeviation As Double
                        stdeviation = xlapp.WorksheetFunction.StDev(xlrange)
                        MsgBox("Standard Deviation: " + stdeviation.ToString)
                        stdev = stdeviation

                        xlbook.Worksheets("Chart").Activate()
                        xlsheet = xlbook.ActiveSheet
                        xlsheet.Range("A1").Value = "StDev" 'I use those cells so I can add the line on the chart
                        xlsheet.Range("B1:Q1").Value = stdeviation

                        xlsheet.ChartObjects("Chart 1").Activate()
                        Dim objchart As Excel.ChartObject = xlsheet.ChartObjects("Chart 1")

                        objchart.Chart.SeriesCollection.Add(xlsheet.Range("B1:K1"))
                        Dim lastSer As Integer = objchart.Chart.SeriesCollection.Count 'so I can get the last SeriesCollection and make the changes on the chart
                        objchart.Chart.SeriesCollection(lastSer).Name = xlsheet.Range("A1").Value
                        objchart.Chart.SeriesCollection(lastSer).ChartType = Excel.XlChartType.xlLine
                    End If

                ElseIf ListBox3.SelectedItem = "Average Deviation" Then
                    If (collection(1).countMetr() * collection.Count) > 30 Then
                        MsgBox("The AveDev function in Excel.Interop cannot process over 30 arguments!")
                    Else
                        Dim avgdeviation As Double
                        avgdeviation = xlapp.WorksheetFunction.AveDev(xlrange)
                        MsgBox("Average Deviation: " + avgdeviation.ToString)
                        avedev = avgdeviation

                        xlbook.Worksheets("Chart").Activate()
                        xlsheet = xlbook.ActiveSheet
                        xlsheet.Range("A2").Value = "AveDev"
                        xlsheet.Range("B2:Q2").Value = avgdeviation

                        xlsheet.ChartObjects("Chart 1").Activate()
                        Dim objchart As Excel.ChartObject = xlsheet.ChartObjects("Chart 1")

                        objchart.Chart.SeriesCollection.Add(xlsheet.Range("B2:K2"))
                        Dim lastSer As Integer = objchart.Chart.SeriesCollection.Count
                        objchart.Chart.SeriesCollection(lastSer).Name = xlsheet.Range("A2").Value
                        objchart.Chart.SeriesCollection(lastSer).ChartType = Excel.XlChartType.xlLine
                    End If
                ElseIf ListBox3.SelectedItem = "Average" Then
                    Dim average As Double
                    average = xlapp.WorksheetFunction.Average(xlrange)
                    MsgBox("Average: " + average.ToString)
                    ave = average

                    xlbook.Worksheets("Chart").Activate()
                    xlsheet = xlbook.ActiveSheet
                    xlsheet.Range("A3").Value = "Average"
                    xlsheet.Range("B3:Q3").Value = average

                    xlsheet.ChartObjects("Chart 1").Activate()
                    Dim objchart As Excel.ChartObject = xlsheet.ChartObjects("Chart 1")

                    objchart.Chart.SeriesCollection.Add(xlsheet.Range("B3:K3"))
                    Dim lastSer As Integer = objchart.Chart.SeriesCollection.Count
                    objchart.Chart.SeriesCollection(lastSer).Name = xlsheet.Range("A3").Value
                    objchart.Chart.SeriesCollection(lastSer).ChartType = Excel.XlChartType.xlLine

                ElseIf ListBox3.SelectedItem = "Median" Then
                    Dim med As Object
                    med = xlapp.WorksheetFunction.Median(xlrange)
                    MsgBox("Median: " + med.ToString)
                    median = med

                    xlbook.Worksheets("Chart").Activate()
                    xlsheet = xlbook.ActiveSheet
                    xlsheet.Range("A4").Value = "Median"
                    xlsheet.Range("B4:Q4").Value = med

                    xlsheet.ChartObjects("Chart 1").Activate()
                    Dim objchart As Excel.ChartObject = xlsheet.ChartObjects("Chart 1")

                    objchart.Chart.SeriesCollection.Add(xlsheet.Range("B4:K4"))
                    Dim lastSer As Integer = objchart.Chart.SeriesCollection.Count
                    objchart.Chart.SeriesCollection(lastSer).Name = xlsheet.Range("A4").Value
                    objchart.Chart.SeriesCollection(lastSer).ChartType = Excel.XlChartType.xlLine

                ElseIf ListBox3.SelectedItem = "Normal Distribution" Then
                    'algorithm for normal distribution, so we could get the bell curve graph
                    Dim mean As Double
                    mean = xlapp.WorksheetFunction.Average(xlrange)
                    Dim stdev As Double
                    stdev = xlapp.WorksheetFunction.StDev(xlrange)
                    Dim mincurve As Double = mean - 3 * stdev
                    Dim mincurve_c As Integer = System.Math.Ceiling(mincurve) 'using ceiling for easier algorithms
                    Dim maxcurve As Double = mean + 3 * stdev
                    Dim maxcurve_c As Integer = System.Math.Ceiling(maxcurve) 'same reason as mincurve_c

                    xlbook.Worksheets("Sheet1").Activate()
                    xlsheet = xlbook.ActiveSheet
                    xlsheet.Range("A1").Value = mincurve_c
                    xlsheet.Range("A2").Value = xlsheet.Range("A1").Value + 1
                    Dim diff As Integer = maxcurve_c - mincurve
                    'fill cells with numbers from mincurve to maxcurve with autofill function
                    Dim destRange As String = "A1:A"
                    destRange += diff.ToString
                    Dim sourceRange As Excel.Range = xlsheet.Range("A1:A2")
                    Dim fillRange As Excel.Range = xlsheet.Range(destRange)
                    sourceRange.AutoFill(fillRange)
                    'fill cells with the normdist for each cell from column A in column B
                    For i As Integer = 1 To diff
                        Dim fromCol As String = "A"
                        Dim toCol As String = "B"
                        fromCol += i.ToString
                        toCol += i.ToString
                        xlsheet.Range(toCol).Value = xlapp.WorksheetFunction.NormDist(xlsheet.Range(fromCol).Value, mean, stdev, False)
                    Next

                    'make the bell curve graph based on the Sheet1 data
                    Dim bellrange As String = "B1:B"
                    bellrange += diff.ToString
                    Dim range As Excel.Range = xlsheet.Range(bellrange).Cells.SpecialCells(Excel.XlCellType.xlCellTypeConstants)
                    Dim bellchart As Excel.ChartObject = xlsheet.ChartObjects.Add(100, 100, 600, 400)
                    bellchart.Chart.SetSourceData(range)
                    bellchart.Chart.SeriesCollection(1).Name = "NormDist"
                    bellchart.Chart.ChartType = Excel.XlChartType.xlXYScatterLinesNoMarkers
                    Dim xRange As Excel.Range = xlsheet.Range(destRange)
                    'bellchart.Chart.SeriesCollection(1).XValues = xRange it does not recognize the XValues method, although it exists in the Excel.Interop documenation
                End If
            End If

            xlbook.Worksheets("Values").Activate()
            xlapp.DisplayAlerts = False
            xlbook.SaveAs(file_path)
            xlbook.Close()
            xlapp.Quit()
            MsgBox("Successfully Excel file saved!")
        Else
            MsgBox("File does not exist!", MsgBoxStyle.Critical, "File Not Found")
        End If

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim saveFile = New SaveFileDialog
        saveFile.Filter = "Text Files (*.txt)|*.txt"
        saveFile.Title = "Save statistics as txt"
        saveFile.InitialDirectory = "C:\"
        saveFile.CheckFileExists = False
        saveFile.CreatePrompt = True
        saveFile.OverwritePrompt = True

        Select Case saveFile.ShowDialog
            Case Windows.Forms.DialogResult.OK
                TextBox1.Text = saveFile.FileName
                Dim txtpath As String = saveFile.FileName
                If File.Exists(txtpath) Then
                    Call writeOnFile(txtpath)
                Else
                    Dim fs As FileStream = File.Create(txtpath)
                    fs.Close()
                    Call writeOnFile(txtpath)
                End If
            Case Windows.Forms.DialogResult.Cancel
                saveFile.Dispose()
        End Select
    End Sub

    Private Sub writeOnFile(path)
        Dim writeStream As StreamWriter
        Dim FindString = File.ReadAllText(path) 'so I can check if there is already the statistic in the .txt
        If FindString.Length > 0 Then
            File.WriteAllText(path, "")
        End If
        writeStream = My.Computer.FileSystem.OpenTextFileWriter(path, True)

        Dim mcount As Integer = 0
        Dim mtime As Integer = 0
        For Each item As samples In collection
            If item.getSex = "M" Then
                mcount += 1
                mtime += item.getTime
            End If
        Next
        avgtimem = mtime / mcount
        writeStream.WriteLine("Average time for males: " + avgtimem.ToString)
            
        Dim wcount As Integer = 0
        Dim wtime As Integer = 0
        For Each item As samples In collection
            If item.getSex = "F" Then
                wcount += 1
                wtime += item.getTime
            End If
        Next
        avgtimefm = wtime / wcount
        writeStream.WriteLine("Average time for females: " + avgtimefm.ToString)
        
        Dim count = 0
        Dim time As Integer = 0
        For Each item As samples In collection
            count += 1
            time += item.getTime
        Next
        avgtimeb = time / count
        writeStream.WriteLine("Average time for both: " + avgtimeb.ToString)
            
        mcount = 0
        Dim mmetr As Integer = 0
        Dim sam As List(Of Integer)
        For Each item As samples In collection
            If item.getSex = "M" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    mcount += 1
                    mmetr += j
                Next
            End If
        Next
        avgmetrm = mmetr / mcount
        writeStream.WriteLine("Average metric for males: " + avgmetrm.ToString)

        wcount = 0
        Dim wmetr As Integer = 0
        For Each item As samples In collection
            If item.getSex = "F" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    wcount += 1
                    wmetr += j
                Next
            End If
        Next
        avgmetrfm = wmetr / wcount
        writeStream.WriteLine("Average metric for females: " + avgmetrfm.ToString)
            
        count = 0
        Dim metr As Integer = 0
        For Each item As samples In collection
            sam = item.getMeas
            For Each j As Integer In sam
                count += 1
                metr += j
            Next
        Next
        avgmetrb = metr / count
        writeStream.WriteLine("Average metric for both: " + avgmetrb.ToString)
           
        Dim minTime As Integer = 2000000000 'I chose that number so it would be close to the maximum number of the Integer range
        For Each item As samples In collection
            If item.getSex = "M" Then
                If (item.getTime < minTime) Then
                    minTime = item.getTime
                End If
            End If
        Next
        mintimem = minTime
        writeStream.WriteLine("Minimum time for males: " + mintimem.ToString)
        
        minTime = 2000000000
        For Each item As samples In collection
            If item.getSex = "F" Then
                If (item.getTime < minTime) Then
                    minTime = item.getTime
                End If
            End If
        Next
        mintimefm = minTime
        writeStream.WriteLine("Minimum time for females: " + mintimefm.ToString)
          
        minTime = 2000000000
        For Each item As samples In collection
            If (item.getTime < minTime) Then
                minTime = item.getTime
            End If
        Next
        mintimeb = minTime
        writeStream.WriteLine("Minimum time for both: " + mintimeb.ToString)

        sam = New List(Of Integer)
        Dim minMetric As Integer = 101 'Because the maximum value it's 100%
        For Each item As samples In collection
            If item.getSex = "M" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    If (j < minMetric) Then
                        minMetric = j
                    End If
                Next
            End If
        Next
        minmetrm = minMetric
        writeStream.WriteLine("Minimum metric for males: " + minmetrm.ToString)
           
        sam = New List(Of Integer)
        minMetric = 101
        For Each item As samples In collection
            If item.getSex = "F" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    If (j < minMetric) Then
                        minMetric = j
                    End If
                Next
            End If
        Next
        minmetrfm = minMetric
        writeStream.WriteLine("Minimum metric for females: " + minmetrfm.ToString)

        sam = New List(Of Integer)
        minMetric = 101
        For Each item As samples In collection
            sam = item.getMeas
            For Each j As Integer In sam
                If (j < minMetric) Then
                    minMetric = j
                End If
            Next
        Next
        minmetrb = minMetric
        writeStream.WriteLine("Minimum metric for both: " + minmetrb.ToString)
            
        Dim maxTime As Integer = -1
        For Each item As samples In collection
            If item.getSex = "M" Then
                If (item.getTime > maxTime) Then
                    maxTime = item.getTime
                End If
            End If
        Next
        maxtimem = maxTime
        writeStream.WriteLine("Maximum time for males: " + maxtimem.ToString)
            
        maxTime = -1
        For Each item As samples In collection
            If item.getSex = "F" Then
                If (item.getTime > maxTime) Then
                    maxTime = item.getTime
                End If
            End If
        Next
        maxtimefm = maxTime
        writeStream.WriteLine("Maximum time for females: " + maxtimefm.ToString)
            
        maxTime = -1
        For Each item As samples In collection
            If (item.getTime > maxTime) Then
                maxTime = item.getTime
            End If
        Next
        maxtimeb = maxTime
        writeStream.WriteLine("Maximum time for both: " + maxtimeb.ToString)

        sam = New List(Of Integer)
        Dim maxMetric As Integer = -1
        For Each item As samples In collection
            If item.getSex = "M" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    If (j > maxMetric) Then
                        maxMetric = j
                    End If
                Next
            End If
        Next
        maxmetrm = maxMetric
        writeStream.WriteLine("Maximum metric for males: " + maxmetrm.ToString)
       
        sam = New List(Of Integer)
        maxMetric = -1
        For Each item As samples In collection
            If item.getSex = "F" Then
                sam = item.getMeas
                For Each j As Integer In sam
                    If (j > maxMetric) Then
                        maxMetric = j
                    End If
                Next
            End If
        Next
        maxmetrfm = maxMetric
        writeStream.WriteLine("Maximum metric for females: " + maxmetrfm.ToString)
            
        sam = New List(Of Integer)
        maxMetric = -1
        For Each item As samples In collection
            sam = item.getMeas
            For Each j As Integer In sam
                If (j > maxMetric) Then
                    maxMetric = j
                End If
            Next
        Next
        maxmetrb = maxMetric
        writeStream.WriteLine("Maximum metric for both: " + maxmetrb.ToString)

        Dim file_name As String = "vbtest.xlsx"
        Dim app_path As String = Directory.GetCurrentDirectory()
        Dim file_path As String = app_path.Replace("\bin\Debug", "\") 'getting back to the directory with the code and file. 
        file_path += file_name 'adding the file name in the path
        If File.Exists(file_path) Then
            Dim xlapp As Excel.Application = New Excel.Application
            Dim xlbook As Excel.Workbook
            Dim xlsheet As Excel.Worksheet
            xlbook = xlapp.Workbooks.Open(file_path)
            xlbook.Worksheets("Values").Activate()
            xlsheet = xlbook.ActiveSheet

            Dim xlrange As Excel.Range
            xlrange = xlsheet.Range("D:Z").Cells.SpecialCells(Excel.XlCellType.xlCellTypeConstants)

            If (collection(1).countMetr() * collection.Count) > 30 Then
                writeStream.WriteLine("Standard Deviation: More than 30 arguments! Excel does not support so many arguments for StDev")
                writeStream.WriteLine("Average Deviation: More than 30 arguments! Excel does not support so many arguments for AveDev")

                Dim med As Object
                med = xlapp.WorksheetFunction.Median(xlrange)
                median = med
                writeStream.WriteLine("Median: " + median.ToString)

                Dim average As Double
                average = xlapp.WorksheetFunction.Average(xlrange)
                ave = average
                writeStream.WriteLine("Mean: " + ave.ToString)

            Else
                Dim stdeviation As Double
                stdeviation = xlapp.WorksheetFunction.StDev(xlrange)
                stdev = stdeviation
                writeStream.WriteLine("Standard Deviation: " + stdev.ToString)

                Dim avgdeviation As Double
                avgdeviation = xlapp.WorksheetFunction.AveDev(xlrange)
                avedev = avgdeviation
                writeStream.WriteLine("Average Deviation: " + avedev.ToString)

                Dim med As Object
                med = xlapp.WorksheetFunction.Median(xlrange)
                median = med
                writeStream.WriteLine("Median: " + median.ToString)

                Dim average As Double
                average = xlapp.WorksheetFunction.Average(xlrange)
                ave = average
                writeStream.WriteLine("Mean: " + ave.ToString)
            End If
            xlbook.Worksheets("Values").Activate()
            xlapp.DisplayAlerts = False
            xlbook.Close()
            xlapp.Quit()
        Else
            MsgBox("Excel file does not exist!")
            writeStream.Close()
        End If
        writeStream.Close()
        
    End Sub
End Class