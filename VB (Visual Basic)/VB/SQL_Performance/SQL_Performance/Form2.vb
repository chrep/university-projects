﻿Imports System.IO
Public Class Form2

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
        Application.Exit()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim openFile As New OpenFileDialog
        openFile.Title = "Please select a file"
        openFile.InitialDirectory = "C:\"
        openFile.Filter = "Text Files (*.txt)|*.txt"

        Select Case openFile.ShowDialog
            Case Windows.Forms.DialogResult.OK
                openFile.CheckFileExists = True
                openFile.CheckPathExists = True
                TextBox1.Text = openFile.FileName
                Dim txt_path As String = TextBox1.Text
                If File.Exists(txt_path) Then
                    Dim fileReader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(txt_path) 'opens a stream to text file
                    Dim lines As String() = Strings.Split(fileReader.ReadToEnd, Environment.NewLine) ' each line of the text
                    Dim list_samples As New List(Of samples)
                    Dim element As String() 'each line separated
                    For i As Integer = 0 To lines.Length - 1
                        element = lines(i).Split(New String() {}, StringSplitOptions.RemoveEmptyEntries) 'element.length -3 is the number of the measurements
                        Dim time As Integer = element(0)
                        Dim age As Integer = element(1)
                        Dim sex As String = element(2)
                        Dim metrics As New List(Of Integer)
                        For j As Integer = 3 To element.Length - 1
                            metrics.Add(element(j))
                        Next
                        Dim sample = New samples(sex, age, time, metrics)
                        list_samples.Add(sample)
                    Next
                    Dim myForm As New Form3(Me, list_samples)
                    MsgBox("File loaded")
                    myForm.Show()
                    Me.Hide()
                Else
                    MsgBox("File does not exist!", MsgBoxStyle.MsgBoxHelp)
                    openFile.Dispose()
                End If

            Case Windows.Forms.DialogResult.Cancel
                openFile.Dispose()
        End Select

    End Sub

End Class