#include "ppm.h"
#include "../Color.h"
#include <iostream>
#include <string>
#include <fstream>
#include "../Image.h"
#include "../Array.h"

using namespace std;


namespace imaging{

	float * ReadPPM(const char * filename, int * w, int * h)
	{

		ifstream input; // ifsteam to access the filename
		cout << "Opening the stream for reading the file" << endl;
		input.open(filename, ios::binary);
		cout << "Successful opening" << endl;

		string sread;
		string format, str_width, str_height, str_maxValue; //use them to read from stream, adding the characters from get() (temp variable)
		unsigned int width, height, maxValue;
		bool flag = true, flag2 = true;

		if (!(input.is_open())) //checks if input is succesfully opened
		{
			cerr << "Cannot open file!" << endl;
			return nullptr;
		}
		else
		{

			//find the format
			while (flag)
			{
				input >> sread;
				cout << "Searching for format" << endl;
				if (!(sread[0] == 'P' && sread[1] == '6'))
				{
					cerr << "NOT P6 format!" << endl;
					return nullptr;
				}
				else
				{
					format += sread[0];
					format += sread[1];
					cout << "Format: " + format << endl;
					flag = false;
				}

			}


			flag = true;

			//find the width
			while (flag)
			{
				input >> sread;
				cout << "Searching for width" << endl;
				str_width = sread;
				cout << "Width: " + str_width << endl;
				flag = false;
			}

			flag = true;

			//find the height
			while (flag)
			{
				input >> sread;
				cout << "Searching for height" << endl;
				str_height = sread;
				cout << "Height: " + str_height << endl;
				flag = false;
			}

			flag = true;

			//find the max value
			while (flag)
			{
				input >> sread;
				cout << "Searching for max value" << endl;
				str_maxValue += sread;
				cout << "MaxValue: " + str_maxValue << endl;
				flag = false;
			}


			width = stoi(str_width);
			height = stoi(str_height);
			maxValue = stoi(str_maxValue);

			if (maxValue > 255)
			{
				cerr << "NOT 24bit!" << endl;
				return nullptr;
			}

		}

		int size = 3 * width * height;
		float * temp_data = new float[size];
		string str_color;
		int i = 0;

		cout << "Parsing the image data" << endl;
		while (input.good() && i<size)
		{
			char color = input.get();
			temp_data[i++] = 1.0f * int(color) / 255; // convert to float	
		}

		*w = width;
		*h = height;

		cout << "Closing the stream" << endl;
		input.close();
		cout << "Succesfull closing" << endl;
		cout << "Returning the image data" << endl;
		return temp_data;

	}

	bool WritePPM(const float * data, int w, int h, const char * filename)
	{

		string name = filename;
		name.erase(name.find_last_of("."), string::npos);
		name += "_neg.ppm";
		cout << "Changed the filename to: " << name << endl;

		cout << "Trying to open the stream for writing" << endl;
		ofstream writeFile(name);
		writeFile.open(filename, ios::binary);
		cout << "Succesfully opened" << endl;


		if (!(writeFile.is_open())) //checks if writeFile is succesfully opened
		{
			cerr << "Cannot open file!" << endl;
			return false;
		}
		else
		{
			cout << "Making the header" << endl;
			writeFile << "P6" << endl << w << endl << h << endl << "255" << endl;
			cout << "Added all the components of the header" << endl;
			cout << "Successfully written the header in the stream" << endl;

			cout << "Parsing the data in a temp data" << endl;
			//get the pixel and write it negative
			int size = 3 * w*h;
			//float* temp_data = (float*)data;
			float *temp_data;
			temp_data = (float*)data;
			cout << "Starting writing data to stream" << endl;
			for (int i = 0; i < size; i++)
			{
				//float r, g, b;
				temp_data[i] = 1.0f - temp_data[i];
				writeFile << (char)(temp_data[i] * 255);
				temp_data[i + 1] = 1.0f - temp_data[i + 1];
				writeFile << (char)(temp_data[i + 1] * 255);
				temp_data[i + 2] = 1.0f - temp_data[i + 2];
				writeFile << (char)(temp_data[i + 2] * 255);
				//writeFile.write((char*)&temp, size);
			}
			cout << "Finished the writing of data" << endl;
			cout << "Closing the stream" << endl;
			writeFile.close();
			cout << "Succesfull closing" << endl;
			return true;

		}

	}

} //namespace imaging