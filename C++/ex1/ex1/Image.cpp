
#include "Image.h"
#include "Color.h"
#include <string>
#include <algorithm>  
#include <iostream>
#include "ppm/ppm.h"
#include "Array.h"
#include "Vec3.h"

using namespace std;

namespace imaging
{

		Vec3<float> Image::getPixel(unsigned int x, unsigned int y)
		{
			Vec3<float> gotPixel = operator()(x, y);
			return gotPixel;
		}


		void Image::setPixel(unsigned int x, unsigned int y, Vec3<float> & value)
		{
			Vec3<float> &gotPixel = operator()(x, y);
			gotPixel = value;
		}


		void Image::setData(const Vec3<float> * & data_ptr)
		{
			if (data_ptr != nullptr && &buffer != nullptr) //checking if data_ptr and buffer exist
			{
				int size = width*height;
				for (int i = 0; i < size; i++)
				{
					buffer[i] = data_ptr[i];
				}
			}
		}


		Image & Image::operator = (const Image & right)
		{
			return (Image &)Array::operator=(right);
		}


		Image::Image() : Array(){}
		

		Image::Image(unsigned int width, unsigned int height) : Array(width, height){}
			

		Image::Image(unsigned int width, unsigned int height, const Vec3<float> * data_ptr) : Array(width, height){}


		Image::Image(const Image &src) : Array(src){}


		Image::~Image(){}


		bool Image::load(const std::string & filename, const std::string & format)
		{
			string name = filename;
			string form = format;
			int *w = (int*) &width;
			int *h = (int*) &height;

			size_t pos = name.find("."); //positiong of "." in name
			string suffix = name.substr(pos); //gets substring from "." till the end
			suffix = suffix.substr(1, std::string::npos); //gets substring after "." till the end

			if (suffix.length() == form.length())
			{
				transform(suffix.begin(), suffix.end(), suffix.begin(), tolower); //transform to lower case
				transform(form.begin(), form.end(), form.begin(), tolower);
				if (suffix.compare(form) == 0)
				{
					if (width) //checks if width is initialized
					{
						width = 0;
					}
					if (height) //checks if height is initialized
					{
						height = 0;
					}
					if (&buffer) //checks if buffer is initialized
					{
						delete &buffer;
					}

					cout << "Starting the ReadPPM" << endl;
					float * temp_data = ReadPPM(filename.c_str(), w, h);

					Vec3<float> * buffer = new Vec3 <float>[width*height];
					for (unsigned int i = 0; i < width * height; i++) {
						buffer[i].r = temp_data[3 * i];
						buffer[i].g = temp_data[3 * i + 1];
						buffer[i].b = temp_data[3 * i + 2];
					}
					cout << "Finished ReadPPM" << endl;
				}
				else
				{
					cout << "Wrong format" << endl;
					return false;
				}

			}
			else
			{
				cout << "Wrong format" << endl;
				return false;
			}
		}


		bool Image::save(const std::string & filename, const std::string & format)
		{
			string name = filename;
			string form = format;

			size_t pos = name.find("."); //position of "." in name
			string suffix = name.substr(pos); //gets substring from "." till the end
			suffix = suffix.substr(1, std::string::npos); //gets substring after "." till the end

			if (suffix.length() == form.length())
			{
				transform(suffix.begin(), suffix.end(), suffix.begin(), tolower); //transform to lower case
				transform(form.begin(), form.end(), form.begin(), tolower);
				if (suffix.compare(form) == 0)
				{
					if (!(width) || !(height) || !(&buffer)) //checks if the Image is NOT initialized
					{
						cout << "Image is not initialized" << endl;
						return false;
					}
					else
					{
						bool saveSucc;
						cout << "Starting the WritePPM" << endl;
						saveSucc = WritePPM((float*) data, width, height, filename.c_str());
						cout << "Finished WritePPM" << endl;
						return saveSucc;
					}
				}
				else
				{
					cout << "Wrong format" << endl;
					return false;
				}

			}
			else
			{
				cout << "Wrong format" << endl;
				return false;
			}
		}

} //namespace imaging