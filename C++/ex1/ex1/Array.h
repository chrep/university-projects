#ifndef _ARR_IMPL_
#define _ARR_IMPL_

#include <vector>

using namespace std;
namespace math
{
	template <typename T>
	class Array
	{
	protected:
		vector<T>  buffer; //the new buffer T
		unsigned int width;
		unsigned int height;

	public:

		//get width of array
		const unsigned int getWidth() const { return width; }

		//get width of height
		const unsigned int getHeight() const { return height; }

		//getting the pointer to data, instead of Color is T
		T *  getRawDataPtr()
		{
			return &buffer[0];
		}

		//constructor with specified width and height
		Array(unsigned int width, unsigned int height)
		{
			this->width = width;
			this->height = height;
		}

		//constructor that copies the source array
		Array(const Array<T> & source) //const so it wont be changed
		{
			width = source.width;
			height = source.height;
			int size = width*height;
			for (int i = 0; i < size; i++)
			{
				buffer[i] = source.buffer[i];
			}
		}

		//gets a reference back to the array[x,y]
		T & operator () (unsigned int x, unsigned int y) 
		{
			if (x >= 0 && x <= width && y >= 0 && y <= height)
			{
				return buffer[x*width + y];
			}
			else
			{
				cout << "Could not find (x,y)" << endl;
				return buffer[0];
			}
		}

		//copy operator that copies the right array
		Array & operator = (const Array<T> & right) //const so it wont be changed
		{
			int size = right.width * right.height;
			for (int i = 0; i < size; i++)
			{
				buffer[i] = right.buffer[i];
			}
			return *this;
		}

		//constructor
		Array() {
			width = 0;
			height = 0;
		}

	};

}
#endif