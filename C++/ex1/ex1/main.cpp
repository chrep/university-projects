#include <iostream>
#include "ppm/ppm.h"
#include <cstdlib>
#include <fstream>
#include "Image.h"

using namespace std;
using namespace imaging;

int main(int argc, char *argv[])
{
	string entry;
	cout << ">";
	cin >> entry;
	string second_arg;

	if (argc < 2) //checks if the filename argument exists or we ask for it
	{
		cout << "File name of the image to load:\n";
		cin >> second_arg;
	}
	else
	{
		second_arg = argv[1];
	}

	const char* filename = second_arg.c_str();
	string form = "ppm";
	const char* format = form.c_str();

	//checks if file exists. If not, the app closes
	ifstream infile(filename);
	if (!(infile))
	{
		cout << "File does not exist!" << endl;
		system("pause");
		return 0;
	}
	infile.close();

	Image* img = new Image();
	bool loadProcess = img->load(filename, format);
	if (loadProcess)
	{
		cout << "Successfull image loading!" << endl;
		cout << "Starting the saving proccess and negative photo" << endl;
		bool saveProcess = img->save(filename, format);
		if (saveProcess)
		{
			cout << "Successfull image saving!" << endl;
			cout << "Image dimensions are:\n" + img->getWidth() + img->getHeight() << endl;
		}
		else
		{
			cout << "Problem on saving" << endl;
		}
	}
	else
	{
		cout << "Problem on loading" << endl;
	}

	system("pause");
	return 0;
}