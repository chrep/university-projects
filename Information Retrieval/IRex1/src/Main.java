/*
 * Christos Magoulas
 * 3120099
 * 
 */

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main 
{

	public static void main(String[] args) 
	{
		
		String txt = getTXT("C:\\Users\\Christos\\workspace\\IRex1\\src\\MED.ALL");
		char[] charArray = txt.toCharArray();
		Documents medDocs = new Documents();
		
		boolean flag1 = true;
		int i = 0;
		int len = charArray.length;



		while (flag1 == true && i<len)
		{
		
			Document doc = new Document();
			
			if  ( charArray[i] == '.' && charArray[i+1] == 'I' && charArray[i+2] == ' ') //checking for the space in i+2 to be sure that .I is not a substring from a content
			{
				int n = i+3;
				boolean numFlag = true;
				StringBuilder numBuilder = new StringBuilder();
				
				while (numFlag == true)
				{
					numBuilder.append(charArray[n]);
					if (charArray[n+1] == '\n')
					{
						numFlag = false;
					}
					else
					{
						n++;
					}
				}
				String num = numBuilder.toString();
				//int id = Integer.parseInt(num);	
				Field fid = new Field("id", num);
				doc.addField(fid);
				i = n + 2;
			}
			if ( charArray[i] == '.' && charArray[i+1] == 'W')
			{
				int j = i+3;
				boolean flag2 = true;
				StringBuilder contentBuilder = new StringBuilder();
				while (flag2 && j<len)
				{
					if  ( charArray[j] == '.' && charArray[j+1] == 'I' && charArray[j+2] == ' ')
					{
						flag2 = false;
						String content = contentBuilder.toString();
						Field fcont = new Field("content", content);
						doc.addField(fcont);
						medDocs.addDocument(doc);
					}
					else 
					{
						contentBuilder.append(charArray[j]);
						j++;
					}
				}
			}
			i++;
		}
		
		try
		{
			File file = new File("C:\\Users\\Christos\\workspace\\IRex1\\src\\xmlfile.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Documents.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			jaxbMarshaller.marshal(medDocs, file);
			jaxbMarshaller.marshal(medDocs, System.out);
		}
		catch (JAXBException e)
		{
			e.printStackTrace();
		} 

	}
	
	
	//get the string version of the file
	private static String getTXT(String path)
	{
		
		StringBuilder contentBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(path)))
		{
			String currentLine;
			while ((currentLine = br.readLine()) != null)
			{
				contentBuilder.append(currentLine).append("\n");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return contentBuilder.toString();
	}

}
