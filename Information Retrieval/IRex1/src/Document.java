/*
 * Christos Magoulas
 * 3120099
 * 
 */

import javax.xml.bind.annotation.*;
import java.util.ArrayList;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "doc")
public class Document
{

	@XmlElement(name="field")
	private ArrayList<Field> fields=null;
	//private Integer id;
	//private String content;
    
    public Document()
    {
    	this.fields = new ArrayList<Field>();
    	
    }
    
    public void addField(Field f)
    {
    	this.fields.add(f);
    }
   
    /*
    public void setId(Integer id) 
    {
        this.id = id;
        
    }
   
    public String getContent() 
    {
        return content;
    }

    public void setContent(String content) 
    {
        this.content = content;
    }
    */

}