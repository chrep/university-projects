/*
 * Christos Magoulas
 * 3120099
 * 
 */

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "field")
public class Field {
	
	@XmlAttribute
	private String name;
	@XmlValue //xml value gets only one value so I have to make one single variable for the id and content
	private String value;
/*	@XmlValue
	private String id;
	private String content; */
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Field()
	{
		this.name = "";
		//this.id = null;
		//this.content = "";
		this.value = "";
	}
	
	public Field(String name, String value) 
	{
		this.name = name;
		this.value = value;
	}
	
/*	public Field(String name, Integer id) 
	{
		this.name = name;
		this.id = id;
	}*/
	
/*	public Field(String name, String content) 
	{
		this.name = name;
		this.content = content;
	}*/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

/*	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}
	

	public void setContent(String content) {
		this.content = content;
	}*/
	
}
