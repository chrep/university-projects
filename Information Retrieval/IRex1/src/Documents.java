/*
 * Christos Magoulas
 * 3120099
 * 
 */

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "add")
@XmlAccessorType (XmlAccessType.FIELD)
public class Documents
{

    public Documents(List<Document> documents) 
    {
        this.documents = documents;
    }
    
    public Documents() 
    {
        this.documents = new ArrayList<Document>();
    }

    @XmlElement(name = "doc")
    private List<Document> documents = null;

    public List<Document> getDocuments() 
    {
        return documents;
    }

    public void setDocuments(List<Document> documents) 
    {
        this.documents = documents;
    }
    
    public void addDocument(Document doc) 
    { 
        this.documents.add(doc);
    }
    
}
