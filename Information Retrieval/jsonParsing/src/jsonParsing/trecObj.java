/*
 * Christos Magoulas
 * 3120099
 * 
 */

package jsonParsing;

public class trecObj {
	
	private int qid;
	private static String q0 = "Q0";
	private String docid;
	private static int rank = 1;
	private double score;
	private static String exp = "STANDARD";
	
	public trecObj(int qid, String docid, double score) 
	{
		this.qid = qid;
		this.docid = docid;
		this.score = score;
	}
	
	public String toString()
	{
		String print_return = this.qid+" "+this.q0+" "+this.docid+" "+this.rank+" "+this.score+" "+this.exp+"\n";
		return print_return;
	}
	
}
