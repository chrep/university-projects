/*
 * Christos Magoulas
 * 3120099
 * 
 */

package jsonParsing;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

public class readJSON {

	public static void main(String[] args) throws IOException {
		
		JSONParser parser = new JSONParser();
		File file = new File("myresults.test");
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		
		try 
		{
			
			for (int counter = 1; counter < 31; counter++)
			{
				String path = "C:\\Users\\Christos\\Desktop\\queries\\q";
				String endfix = Integer.toString(counter)+".txt";
				path+=endfix;
				Object ob = parser.parse(new FileReader(path));
				JSONObject job = (JSONObject) ob;
				
				JSONObject response = (JSONObject) job.get("response");
				JSONArray docs = (JSONArray) response.get("docs");
				Iterator i = docs.iterator();
				
				while (i.hasNext())
				{
					JSONObject doc = (JSONObject) i.next();
					String docid = (String) doc.get("id");
					double score = (double) doc.get("score");
					trecObj trecob = new trecObj(counter,docid,score);
					writer.write(trecob.toString());
				}
			}
			writer.flush();
			
		}
		catch (Exception e)
		{
			System.err.println("Error: " + e.getMessage());
		}
		
		writer.close();
		
	}

}
