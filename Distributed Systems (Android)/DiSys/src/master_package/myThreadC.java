package master_package;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import master_package.DirectionsObject;
import master_package.MapWorker_1;
import master_package.MapWorker_2;
import master_package.MapWorker_3;
import master_package.ReduceWorker;
import master_package.myThread;

public class myThreadC extends myThread {
	
	public  myThreadC (Tuple<String,DirectionsObject> y) {
		Socket requestSocket3 = null;
		ObjectOutputStream out3 = null;
		ObjectInputStream in3 = null;
		
		try {
			requestSocket3 = new Socket(InetAddress.getByName("10.0.0.8"), 7779);
			System.out.println("Connected with MapWorker_3.");
			out3 = new ObjectOutputStream(requestSocket3.getOutputStream());
			in3 = new ObjectInputStream(requestSocket3.getInputStream());

			out3.writeUTF("Greetings from Master!");
			out3.flush();
			
			//Dokimh gia ton Mapper_3
			out3.writeObject(y);
			out3.flush();
			
			System.out.println(in3.readBoolean());
			System.out.println(in3.readUTF());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
