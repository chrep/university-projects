package master_package;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.net.*;
import master_package.Tuple;

public class ReduceWorker implements Worker {
	
	public static void main(String[] args) throws ClassNotFoundException {
		new ReduceWorker().openServer();
	}
	
	//initialize()
	public void openServer() throws ClassNotFoundException {
		ServerSocket providerSocket1 = null;
		ServerSocket providerSocket2 = null;
		ServerSocket providerSocket3 = null;
		//ServerSocket providerSocket4 = null;
		Socket connectMapper1 = null;
		Socket connectMapper2 = null;
		Socket connectMapper3 = null;
		//Socket connectMaster = null;
		Socket requestSocketMaster = null;
		ObjectOutputStream outMaster = null;
		ObjectInputStream inMaster = null;
		
		//------Connection me tous MapWorkers kai ton Master------
		try {
			providerSocket1 = new ServerSocket(8002);
			System.out.println("Connection Reducer-Mapper1 is initialized.");
			
			providerSocket2 = new ServerSocket(8003);
			System.out.println("Connection Reducer-Mapper2 is initialized.");
			
			providerSocket3 = new ServerSocket(8004);
			System.out.println("Connection Reducer-Mapper3 is initialized.");
			
			requestSocketMaster = new Socket(InetAddress.getByName("10.0.0.8"), 8001);
			System.out.println("Reducer has been successfully initialized!");
			outMaster = new ObjectOutputStream(requestSocketMaster.getOutputStream());
			inMaster = new ObjectInputStream(requestSocketMaster.getInputStream());

			outMaster.writeUTF("Hello Master from Reducer!");
			outMaster.flush();
			System.out.println(inMaster.readUTF());
			waitForMasterAck();
				
			while (true) {
				connectMapper1 = providerSocket1.accept();
				connectMapper2 = providerSocket2.accept();
				connectMapper3 = providerSocket3.accept();
				
				//------Inputs to receive data from the mappers------		
				ObjectInputStream in1 = new ObjectInputStream(connectMapper1.getInputStream());
				ObjectInputStream in2 = new ObjectInputStream(connectMapper2.getInputStream());
				ObjectInputStream in3 = new ObjectInputStream(connectMapper3.getInputStream());
				
				//------Input apo tous MapWorkers kai reduce procedure------
				System.out.println(in1.readUTF());
				System.out.println(in2.readUTF());
				System.out.println(in3.readUTF());
				
				System.out.println("Begin fetching process:");
				ArrayList<Tuple<String,DirectionsObject>> input1 = (ArrayList<Tuple<String, DirectionsObject>>) in1.readObject();
				System.out.println("Got the results from MapWorker_1.");
				ArrayList<Tuple<String,DirectionsObject>> input2 = (ArrayList<Tuple<String, DirectionsObject>>) in2.readObject();
				System.out.println("Got the results from MapWorker_2.");
				ArrayList<Tuple<String,DirectionsObject>> input3 = (ArrayList<Tuple<String, DirectionsObject>>) in3.readObject();
				System.out.println("Got the results from MapWorker_3.");
				System.out.println("Successfully received the results from all the MapWorkers!");
				
				//Neos pinakas pou periexei tous 3 apo tous Mappers gia na vgalei ta 2pla
				ArrayList<Tuple<String,DirectionsObject>> Inputs = new ArrayList<Tuple<String,DirectionsObject>>();
				if(input1.size()==0 && input2.size()==0 & input3.size()==0) {
					Inputs = null;
					outMaster.writeObject(Inputs);
					outMaster.flush();
				} else {
					for(int i=0; i<input1.size(); i++) {
						Inputs.add(input1.get(i));
					}
					for(int i=0; i<input2.size(); i++) {
						Inputs.add(input2.get(i));
					}
					for(int i=0; i<input3.size(); i++) {
						Inputs.add(input3.get(i));
					}
					outMaster.writeObject(sendResults(Inputs));
					outMaster.flush();
				}
				
				//------Kleisimo twn streams kai sockets------
				in1.close();
				connectMapper1.close();
				in2.close();
				connectMapper2.close();
				in3.close();
				connectMapper3.close();
				outMaster.close();
				inMaster.close();
				
			}
	
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				providerSocket1.close();
				providerSocket2.close();
				providerSocket3.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	
	//------Print otan paroume to ack apo ton Master------
	public void waitForMasterAck() {
		System.out.println("Received Ack from Master.");
	}
	
	//------Ta3inomhsh kai diagrafh diplotypwn------
	public ArrayList<Tuple<String,DirectionsObject>> sendResults(ArrayList<Tuple<String,DirectionsObject>> Inputs) {
		for(int i=0; i<Inputs.size(); i++) {
			for(int j=i+1; j<Inputs.size(); j++) {
				if(Inputs.get(i)==Inputs.get(j)) {
					Inputs.remove(j);
				}
			}
		}
		return Inputs;
	}
	
	//TO-DO list
	public void waitForTasksThread() {}
	
}