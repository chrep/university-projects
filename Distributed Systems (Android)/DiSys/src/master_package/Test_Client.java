package master_package;
import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class Test_Client extends Thread {
	
	public static void main(String[] args) throws EOFException, ClassNotFoundException {
		new Test_Client().startClient();
	}
	public void startClient() throws ClassNotFoundException {
		//Socket kai streams tou Master
		Socket requestSocketMaster = null;
		ObjectOutputStream outm = null;
		ObjectInputStream inm = null;
		String message = "Map-Reduce procedure is complete.";
		try {
			//Epikoinwnia me Master
			requestSocketMaster = new Socket(InetAddress.getByName("192.168.1.6"), 8000);
			System.out.println("Client-Master communication has been initialized.");
			outm = new ObjectOutputStream(requestSocketMaster.getOutputStream());
			inm = new ObjectInputStream(requestSocketMaster.getInputStream());

			outm.writeUTF("Greetings from Client!");
			outm.flush();
			
			//------To tuple pou dinei o client ston master------
			ArrayList<String> t = new ArrayList<String>();
			//DirectionsObject t1 = new DirectionsObject(t);
			//Tuple<String,DirectionsObject> x = new Tuple<String,DirectionsObject>("test", t1);
			//outm.writeObject(x); 
			
			
			ArrayList<Tuple<String,DirectionsObject>> result = (ArrayList<Tuple<String, DirectionsObject>>) inm.readObject();
			System.out.println("Got'em");
			System.out.println(inm.readUTF());
			System.out.println("Server> "+message);
			
			inm.close();
			outm.close();
			requestSocketMaster.close();
			
			
		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} 
		
	}
}