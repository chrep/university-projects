package master_package;

public class myThreadRunnable implements Runnable{

	/* Define a string variable with name input*/
	String input;

	/* Define the constructor of the class that assigns the string*/
	public myThreadRunnable(String inp) {
		this.input = inp;
	}

	public void run() {
		for (int i = 0; i < 1000; i++) {
			System.err.println(i + ":\t" + input);
			try {
				Thread.sleep((int) (Math.random() * 500));
			} catch (InterruptedException e) {}
		}
	}

	public static void main(String[] args) {
		myThreadRunnable t = new myThreadRunnable("Distributed");
		myThreadRunnable t2 = new myThreadRunnable("Systems");
		
		//run the threads
		new Thread(t).start();
		new Thread(t2).start();
		
	}
}
