package master_package;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import master_package.DirectionsObject;
import master_package.MapWorker_1;
import master_package.MapWorker_2;
import master_package.MapWorker_3;
import master_package.ReduceWorker;

public class Master extends Thread implements Runnable, Serializable {
	//Memory that holds the last 100 DirectionsObjects that were searched
	private ArrayList<DirectionsObject> Cache = new ArrayList<DirectionsObject>(100);
	
	//TO-DO List
	public void waitForNewQueriesThread() {}
	public DirectionsObject askGoogleDirectionsAPI(String x) {return null;}
	
	//Print ack otan teleiwsoun oi Mappers gia na arxisei o Reducer
	public void ackToReducers() {
		System.out.println("Sending Ack to Reducer to start the Reduce-Procedure.");
	}
	
	//Print otan steiloume to tuple stous Mappers
	public void distributeToMappers() {
		System.out.println("Tuple sent to MapWorkers.");
	}
	
	//Print otan steiloume to apotelesma tou map-reduce ston client
	public void sendResultsToClient() {
		System.out.print("Map-Reduce complete. Data sent to Client.");
	}
	
	//Print otan pairnoume data apo ton reducer
	public void collectDataFromReducer() {
		System.out.println("Collected data from Reducer successfully!");
	}
	
	//Print otan h vash ginetai updated
	public boolean updateDatabase() {
		System.out.println("Database has been updated succesfully.");
		return true;
	}
	
	//Test main gia epikoinwnia me ton Test_Client
	public static void main(String[] args) throws ClassNotFoundException, EOFException, InterruptedException {
		new Master().openServer();
		
	}
	
	//H antistoixh initialize tou skeletou
	public void openServer() throws ClassNotFoundException, InterruptedException {
		ServerSocket providerSocket = null;
		Socket connection = null;
		//Socket kai streams tou Reducer
		ServerSocket providerSocketR = null;
		Socket connectionR = null;
		
		try {
				//Vazoume ton ari8mo tou port pou 8eloume gia ton client
				providerSocket = new ServerSocket(8000);
				System.out.println("Master-Client connection is initialized.");
				//Reducer
				providerSocketR = new ServerSocket(8001);
				System.out.println("Master-Reducer communication is initialized.");

				while (true) {
					connection = providerSocket.accept();
					System.out.println("Successful connection.");
					ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
					ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
						
					
					
					ObjectOutputStream outR;
					ObjectInputStream inR;
					try {
					/*	Tuple<String, DirectionsObject> readObject = (Tuple<String, DirectionsObject>) in.readObject();
						Tuple<String,DirectionsObject> x =  readObject; 
					*/
						
						System.out.println("Test!");
						Tuple<String, DirectionsObject> x = (Tuple<String, DirectionsObject>) in.readObject();
						
							
						//------connection me ton reducer------
						connectionR = providerSocketR.accept();
						System.out.println("Successful connection with Reducer.");
						outR = new ObjectOutputStream(connectionR.getOutputStream());
						inR = new ObjectInputStream(connectionR.getInputStream());
						

						
						//Anoigei to client gia epikoinwnia me tous MapWorkers
						new Master().startClient(x);
						ackToReducers();
						outR.writeUTF("Start Reduce procedure.");
						outR.flush();
						
						System.out.println(inR.readUTF());
						
						//Apotelesmata apo ton Reducer
						ArrayList<Tuple<String,DirectionsObject>> result = (ArrayList<Tuple<String, DirectionsObject>>) inR.readObject();
						collectDataFromReducer();
						
						if(result==null) {
							System.out.println("Ask google plz");
							out.writeObject(null);
							out.flush();
						} else {
							out.writeObject(result.get(0));
							out.flush();
						}
						
						out.writeUTF("Procedure done.");
						out.flush();
						//Kleinei ta stream kai ta sockets
						inR.close();
						outR.close();
						in.close();
						out.close();
						connectionR.close();
						connection.close();
					} catch (IOException ioException) {
						ioException.printStackTrace();
					}
					
					
				}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		
		} finally {
			try {
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	public void run1() {
		// TODO Auto-generated method stub
			
	}
	
	//Emfanizei oti oi MapWorkers teleiwsan afou epistre4oun true
	public void waitForMappers() {
		System.out.println("All the MapWorkers have finished!");
	}
	
	public void startClient(Tuple<String,DirectionsObject> y) throws EOFException, ClassNotFoundException, InterruptedException {
			//Sent to MapWorkers
			distributeToMappers();
			
			myThread i = new myThread (y);
			i.start();
			sleep(1000);
			waitForMappers();
			//-----------------------------------------Telos MapWorkers----------------------------
			

	}
	
	//Search Cache memory for DirectionsObject
	public DirectionsObject search_cache(DirectionsObject x) {
		//Check if cache memory is empty
		if(Cache.isEmpty()) {
			System.out.println("Cache is empty!");
			return null;
		}
		int mege8os_array = Cache.size();
		boolean item_found = false;
		//Check cache for object and return position if found
		for(int i = 0; i<mege8os_array; i++) {
			if(x==Cache.get(i)) {
				System.out.println("The object was found in Cache.");
				item_found = true;
				return Cache.get(i);
			}
		}
		//Return 0 if object not found
		if(item_found==false) {
			System.out.println("The object was not found in Cache.");
			return null;
		}
		return null;
	}
	//------Import a new Object in cache and check if it is full------
	public void import_cache(DirectionsObject x) {
		//Check if cache is full
		if(Cache.size()==100) {
			System.out.println("Cache memory is full!");
			Cache.remove(0);
			for(int i=1; i<Cache.size()-1; i++) {
				Cache.set(i, Cache.get(i+1));
			}
			Cache.add(x);
			boolean supd = updateDatabase();
		}
		else {
			Cache.add(x);
			boolean supd = updateDatabase();
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
}