package master_package;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import master_package.DirectionsObject;
import master_package.MapWorker_1;
import master_package.MapWorker_2;
import master_package.MapWorker_3;
import master_package.ReduceWorker;
import master_package.myThread;

public class myThreadB extends myThread {
	
	public  myThreadB (Tuple<String,DirectionsObject> y) {
		Socket requestSocket2 = null;
		ObjectOutputStream out2 = null;
		ObjectInputStream in2 = null;
		
		try {
			requestSocket2 = new Socket(InetAddress.getByName("10.0.0.8"), 7778);
			System.out.println("Connected with MapWorker_2.");
			out2 = new ObjectOutputStream(requestSocket2.getOutputStream());
			in2 = new ObjectInputStream(requestSocket2.getInputStream());

			out2.writeUTF("Greetings from Master!");
			out2.flush();
			
			//Dokimh gia ton Mapper_2
			out2.writeObject(y);
			out2.flush();
			
			System.out.println(in2.readBoolean());
			System.out.println(in2.readUTF());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
