package master_package;
import master_package.Tuple;
import java.io.*;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import master_package.HashTextTest;

public class MapWorker_2 implements Runnable, Worker {
	public String IP_address = "10.0.0.8";
	public String Port_number = "7778";
	//------Memory tou MapWorker_2------
	private ArrayList<Tuple<String,DirectionsObject>> MapCache2 = new ArrayList<Tuple<String,DirectionsObject>>(100);
	
	
	public void startCache() {
		
		//------Gemisma Mnhmhs Cache2------
		ArrayList<String> x4 = new ArrayList<String>();
		x4.add(new String("37.980313,23.733019"));
		x4.add(new String("37.980286,23.732965"));
		x4.add(new String("37.983591,23.730134"));
		x4.add(new String("37.983776,23.729924"));
		x4.add(new String("37.983996,23.728824"));
		x4.add(new String("37.984133,23.728564"));
		x4.add(new String("37.984201,23.728694"));
		DirectionsObject d4 = new DirectionsObject(x4);
		String nd4 = "37.980313,23.733019,37.984201,23.728694";
		Tuple<String,DirectionsObject> t4 = new Tuple<String,DirectionsObject>(nd4,d4);
		MapCache2.add(t4);
		
		ArrayList<String> x5 = new ArrayList<String>();
		x5.add(new String("37.976723,23.720712"));
		x5.add(new String("37.976620,23.720840"));
		x5.add(new String("37.976720,23.720903"));
		x5.add(new String("37.976964,23.721030"));
		x5.add(new String("37.977099,23.721097"));
		x5.add(new String("37.977374,23.720622"));
		x5.add(new String("37.977473,23.720381"));
		x5.add(new String("37.977682,23.718871"));
		x5.add(new String("37.977847,23.717533"));
		x5.add(new String("37.977678,23.715685"));
		x5.add(new String("37.977600,23.715596"));
		x5.add(new String("37.977541,23.715129"));
		x5.add(new String("37.977662,23.715067"));
		x5.add(new String("37.977821,23.714895"));
		x5.add(new String("37.977147,23.713433"));
		x5.add(new String("37.977316,23.713224"));
		x5.add(new String("37.977845,23.712299"));
		x5.add(new String("37.978217,23.711712"));
		x5.add(new String("37.978550,23.711562"));
		DirectionsObject d5 = new DirectionsObject(x5);
		String nd5 = "37.976723,23.720712,37.978550,23.711562";
		Tuple<String,DirectionsObject> t5 = new Tuple<String,DirectionsObject>(nd5,d5);
		MapCache2.add(t5);
		
		ArrayList<String> x6 = new ArrayList<String>();
		x6.add(new String("37.976102,23.725625"));
		x6.add(new String("37.976203,23.725573"));
		x6.add(new String("37.976126,23.724724"));
		x6.add(new String("37.975784,23.724714"));
		x6.add(new String("37.975938,23.723415"));
		x6.add(new String("37.976308,23.722189"));
		x6.add(new String("37.976968,23.721047"));
		x6.add(new String("37.976607,23.720825"));
		x6.add(new String("37.976719,23.720705"));
		DirectionsObject d6 = new DirectionsObject(x6);
		String nd6 = "37.976102,23.725625,37.976719,23.720705";
		Tuple<String,DirectionsObject> t6 = new Tuple<String,DirectionsObject>(nd6,d6);
		MapCache2.add(t6);
	}
	
	//TO-DO List
	public void waitForTasksThread() {}
	
	//------Print otan steiloume ton pinaka me ta tuple ston Reducer------
	public void sendToReducers(ArrayList<Tuple<String,DirectionsObject>> x) {
		System.out.println("Results sent to Reducer.");
	}

	//------Tsekaroume thn mnhmh tou MapWorker gia to Tuple------ 
	public ArrayList<Tuple<String,DirectionsObject>> map(Tuple<String,DirectionsObject> x) {
		ArrayList<Tuple<String,DirectionsObject>> pinakas = new ArrayList<Tuple<String,DirectionsObject>>();
		for(int i=0; i<MapCache2.size(); i++) {
			if(MapCache2.get(i)==x) {
				pinakas.add(MapCache2.get(i));
			}
		}
		if(pinakas.size()==0) {
			System.out.println("MapWorker_2 did not find the Tuple.");
			return pinakas;
		}
		else {
			System.out.println("MapWorker_2 found the Tuple!");
			return pinakas;
		}	
	}
	
	//------Notify Master when mapping is done------
	public boolean notify_master() {
		System.out.println("Worker_2 mapping is complete.");
		boolean x = true;
		return x;
	}
	//------Ypologizei to hash enos string mesw ths klashs HashTextTest------
	public String calculateHash(String x) throws NoSuchAlgorithmException {
		HashTextTest hasher = new HashTextTest("Hasher");
		return hasher.sha1(x);
	}
	
	public MapWorker_2() {
		startCache();
	}
	
	//------Main gia epikoinwnia me ton Master------
	public static void main(String[] args) throws ClassNotFoundException {
		MapWorker_2 m2 = new MapWorker_2();
		m2.openServer();
	}
	
	//------H antistoixh initialize tou skeletou------
	public void openServer() throws ClassNotFoundException {
		ServerSocket providerSocket = null;
		Socket connection = null;
		//Socket kai streams tou Reducer
		Socket requestSocketR = null;
		ObjectOutputStream outR = null;

		try {
				//Vazoume ton ari8mo tou port pou 8eloume gia ton client
				providerSocket = new ServerSocket(7778);
				System.out.println("MapWorker_2 is initialized!");

				while (true) {
					//------Syndesh me ton Master------
					connection = providerSocket.accept();
					System.out.println("Successful connection.");
					ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
					ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
					
					System.out.println(in.readUTF());
					
					//dinei stoixeia ston master gia to hash
					String hash_map2;
					try {
						hash_map2 = calculateHash(IP_address+Port_number);
						out.writeUTF(hash_map2);
						out.flush();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//--------MAP procedure------------
					Tuple<String,DirectionsObject> incm = (Tuple<String,DirectionsObject>) in.readObject();
					String hashincm;
					try {
						hashincm = calculateHash(incm.getKey());
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ArrayList<Tuple<String,DirectionsObject>> apotelesma = map(incm);
					
					//Ack true ston Master
					boolean ack2 = notify_master();
					out.writeBoolean(ack2);
					out.writeUTF("MapWorker_2 is done!");
					out.flush();
					in.close();
					out.close();
					
					//Epikoinwnia me Reducer
					requestSocketR = new Socket(InetAddress.getByName("10.0.0.8"), 8003);
					System.out.println("Communication with Reducer has been initialized.");
					outR = new ObjectOutputStream(requestSocketR.getOutputStream());
					outR.writeUTF("Hello Reducer from MapWorker_2.");
					
					//------Stelnoume to apotelesma ston Reducer------
					sendToReducers(apotelesma);
					outR.writeObject(apotelesma);
					outR.flush();
					
					//------Telos tou MapWorker_2------
					outR.close();
					connection.close();
				}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}