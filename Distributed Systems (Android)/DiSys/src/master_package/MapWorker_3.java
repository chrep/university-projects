package master_package;
import master_package.Tuple;
import java.io.*;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import master_package.HashTextTest;

public class MapWorker_3 implements Runnable, Worker {
	public String IP_address = "10.0.0.8";
	public String Port_number = "7779";
	//------Memory tou MapWorker_3------
	private ArrayList<Tuple<String,DirectionsObject>> MapCache3 = new ArrayList<Tuple<String,DirectionsObject>>(100);
	
	public void startCache() {
		
		//------Gemisma Mnhmhs Cache3------
		ArrayList<String> x7 = new ArrayList<String>();
		x7.add(new String("37.976719,23.720705"));
		x7.add(new String("37.976607,23.720825"));
		x7.add(new String("37.976377,23.720552"));
		x7.add(new String("37.976152,23.720071"));
		x7.add(new String("37.975883,23.719631"));
		x7.add(new String("37.975791,23.719576"));
		x7.add(new String("37.975563,23.719536"));
		x7.add(new String("37.975524,23.719533"));
		x7.add(new String("37.975240,23.719617"));
		x7.add(new String("37.974673,23.719829"));
		x7.add(new String("37.974517,23.719860"));
		x7.add(new String("37.974319,23.719948"));
		x7.add(new String("37.974067,23.720066"));
		x7.add(new String("37.973925,23.719570"));
		x7.add(new String("37.973836,23.719015"));
		x7.add(new String("37.974149,23.718867"));
		x7.add(new String("37.973969,23.718540"));
		x7.add(new String("37.973942,23.718470"));
		x7.add(new String("37.974005,23.718419"));
		x7.add(new String("37.973895,23.718108"));
		x7.add(new String("37.973535,23.718167"));
		DirectionsObject d7 = new DirectionsObject(x7);
		String nd7 = "37.976719,23.720705,37.973535,23.718167";
		Tuple<String,DirectionsObject> t7 = new Tuple<String,DirectionsObject>(nd7,d7);
		MapCache3.add(t7);
		
		ArrayList<String> x8 = new ArrayList<String>();
		x8.add(new String("37.976102,23.725625"));
		x8.add(new String("37.976097,23.725750"));
		x8.add(new String("37.975994,23.725872"));
		x8.add(new String("37.976003,23.725908"));
		x8.add(new String("37.975998,23.725988"));
		x8.add(new String("37.975914,23.726467"));
		x8.add(new String("37.975687,23.727383"));
		x8.add(new String("37.975239,23.727256"));
		x8.add(new String("37.975003,23.727206"));
		x8.add(new String("37.974847,23.727622"));
		x8.add(new String("37.974809,23.727740"));
		x8.add(new String("37.974613,23.728189"));
		x8.add(new String("37.974533,23.728355"));
		x8.add(new String("37.974505,23.728427"));
		x8.add(new String("37.974257,23.728296"));
		x8.add(new String("37.973825,23.728087"));
		x8.add(new String("37.973591,23.727933"));
		x8.add(new String("37.973584,23.727929"));
		DirectionsObject d8 = new DirectionsObject(x8);
		String nd8 = "37.976102,23.725625,37.973584,23.727929";
		Tuple<String,DirectionsObject> t8 = new Tuple<String,DirectionsObject>(nd8,d8);
		MapCache3.add(t8);
	
	}
	
	//TO-DO List
	public void waitForTasksThread() {}
	
	//------Print otan steiloume ton pinaka me ta tuple ston Reducer------
	public void sendToReducers(ArrayList<Tuple<String,DirectionsObject>> x) {
		System.out.println("Results sent to Reducer.");
	}

	//------Tsekaroume thn mnhmh tou MapWorker gia to Tuple------ 
	public ArrayList<Tuple<String,DirectionsObject>> map(Tuple<String,DirectionsObject> x) {
		ArrayList<Tuple<String,DirectionsObject>> pinakas = new ArrayList<Tuple<String,DirectionsObject>>();
		for(int i=0; i<MapCache3.size(); i++) {
			if(MapCache3.get(i)==x) {
				pinakas.add(MapCache3.get(i));
			}
		}
		if(pinakas.size()==0) {
			System.out.println("MapWorker_3 did not find the Tuple.");
			return pinakas;
		}
		else {
			System.out.println("MapWorker_3 found the Tuple!");
			return pinakas;
		}	
	}
	
	//------Notify Master when mapping is done------
	public boolean notify_master() {
		System.out.println("Worker_3 mapping is complete.");
		boolean x = true;
		return x;
	}
	//------Ypologizei to hash enos string mesw ths klashs HashTextTest------
	public String calculateHash(String x) throws NoSuchAlgorithmException {
		HashTextTest hasher = new HashTextTest("Hasher");
		return hasher.sha1(x);
	}
	
	public MapWorker_3() {
		startCache();
	}
	
	//------Main gia epikoinwnia me ton Master------
	public static void main(String[] args) throws ClassNotFoundException {
		MapWorker_3 m3 = new MapWorker_3();
		m3.openServer();
	}
	//------H antistoixh initialize tou skeletou------
	public void openServer() throws ClassNotFoundException {
		ServerSocket providerSocket = null;
		Socket connection = null;
		//Socket kai streams tou Reducer
		Socket requestSocketR = null;
		ObjectOutputStream outR = null;
		
		try {
				//Vazoume ton ari8mo tou port pou 8eloume gia ton client
				providerSocket = new ServerSocket(7779);
				System.out.println("MapWorker_3 is initialized!");

				while (true) {
					//------Syndesh me ton Master------
					connection = providerSocket.accept();
					System.out.println("Successful connection.");
					ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
					ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
					
					System.out.println(in.readUTF());
					
					//dinei stoixeia ston master gia to hash
					String hash_map3;
					try {
						hash_map3 = calculateHash(IP_address+Port_number);
						out.writeUTF(hash_map3);
						out.flush();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					//--------MAP procedure------------
					Tuple<String,DirectionsObject> incm = (Tuple<String,DirectionsObject>) in.readObject();
					String hashincm;
					try {
						hashincm = calculateHash(incm.getKey());
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ArrayList<Tuple<String,DirectionsObject>> apotelesma = map(incm);
					
					//Ack true ston Master
					boolean ack3 = notify_master();
					out.writeBoolean(ack3);
					out.writeUTF("MapWorker_3 is done!");
					out.flush();
					in.close();
					out.close();
					
					//Epikoinwnia me Reducer
					requestSocketR = new Socket(InetAddress.getByName("10.0.0.8"), 8004);
					System.out.println("Communication with Reducer has been initialized.");
					outR = new ObjectOutputStream(requestSocketR.getOutputStream());
					outR.writeUTF("Hello Reducer from MapWorker_3.");
					
					//------Stelnoume to apotelesma ston Reducer------
					sendToReducers(apotelesma);
					outR.writeObject(apotelesma);
					outR.flush();
					
					//------Telos tou MapWorker_3------
					outR.close();
					connection.close();
				}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}