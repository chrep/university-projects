package master_package;
import master_package.DirectionsObject;
import master_package.Tuple;
import master_package.HashTextTest;
import java.io.*;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class MapWorker_1 implements Runnable, Worker {
	public String IP_address = "10.0.0.8";
	public String Port_number = "7777";
	//------Memory of MapWorker_1------
	private ArrayList<Tuple<String,DirectionsObject>> MapCache1 = new ArrayList<Tuple<String,DirectionsObject>>(100);
	
	
	
	public void startCache() {
		
		//------Gemisma Mnhmhs Cache1------
		ArrayList<String> x1 = new ArrayList<String>();
		x1.add(new String("37.976136,23.725623"));
		x1.add(new String("37.975996,23.725859"));
		x1.add(new String("37.976007,23.725989"));
		x1.add(new String("37.976313,23.726207"));
		x1.add(new String("37.976288,23.726566"));
		x1.add(new String("37.976254,23.726777"));
		x1.add(new String("37.976095,23.727474"));
		x1.add(new String("37.975865,23.728308"));
		x1.add(new String("37.975719,23.728852"));
		x1.add(new String("37.975415,23.730502"));
		x1.add(new String("37.975294,23.731843"));
		x1.add(new String("37.975150,23.733214"));
		x1.add(new String("37.975074,23.733882"));
		x1.add(new String("37.974820,23.735575"));
		x1.add(new String("37.975096,23.735630"));
		x1.add(new String("37.975098,23.735681"));
		DirectionsObject d1 = new DirectionsObject(x1);
		String nd1 = "37.976136,23.725623,37.975098,23.735681";
		Tuple<String,DirectionsObject> t1 = new Tuple<String,DirectionsObject>(nd1,d1);
		MapCache1.add(t1);
		
		ArrayList<String> x2 = new ArrayList<String>();
		x2.add(new String("37.975098,23.735681"));
		x2.add(new String("37.975096,23.735630"));
		x2.add(new String("37.973418,23.735212"));
		x2.add(new String("37.970634,23.732591"));
		x2.add(new String("37.970177,23.731722"));
		x2.add(new String("37.969626,23.731598"));
		x2.add(new String("37.969491,23.729970"));
		x2.add(new String("37.969258,23.729841"));
		x2.add(new String("37.968778,23.729632"));
		x2.add(new String("37.968807,23.729522"));
		DirectionsObject d2 = new DirectionsObject(x2);
		String nd2 = "37.975098,23.735681,37.968807,23.729522";
		Tuple<String,DirectionsObject> t2 = new Tuple<String,DirectionsObject>(nd2,d2);
		MapCache1.add(t2);
		
		ArrayList<String> x3 = new ArrayList<String>();
		x3.add(new String("37.975098,23.735681"));
		x3.add(new String("37.975096,23.735630"));
		x3.add(new String("37.976028,23.735775"));
		x3.add(new String("37.976818,23.735874"));
		x3.add(new String("37.977550,23.735316"));
		x3.add(new String("37.979267,23.733825"));
		x3.add(new String("37.980286,23.732965"));
		x3.add(new String("37.980313,23.733019"));
		DirectionsObject d3 = new DirectionsObject(x3);
		String nd3 = "37.975098,23.735681,37.980313,23.733019";
		Tuple<String,DirectionsObject> t3 = new Tuple<String,DirectionsObject>(nd3,d3);
		MapCache1.add(t3);
	}
	//TO-DO List
	public void waitForTasksThread() {}
	
	//------Print otan steiloume ton pinaka me ta tuple ston Reducer------
	public void sendToReducers(ArrayList<Tuple<String,DirectionsObject>> x) {
		System.out.println("Results sent to Reducer.");
	}
	
	//------Tsekaroume thn mnhmh tou MapWorker gia to Tuple------
	public ArrayList<Tuple<String,DirectionsObject>> map(Tuple<String,DirectionsObject> x) {
		ArrayList<Tuple<String,DirectionsObject>> pinakas = new ArrayList<Tuple<String,DirectionsObject>>();
		for(int i=0; i<MapCache1.size(); i++) {
			if(MapCache1.get(i)==x) {
				pinakas.add(MapCache1.get(i));
			}
		}
		if(pinakas.size()==0) {
			System.out.println("MapWorker_1 did not find the Tuple.");
			return pinakas;
		}
		else {
			System.out.println("MapWorker_1 found the Tuple!");
			return pinakas;
		}
		
	}
	
	//------Notify Master when mapping is done------
	public static boolean notify_master() {
		System.out.println("Worker_1 mapping is complete.");
		boolean x = true;
		return x;
	}
	
	//------Ypologizei to hash enos string mesw ths klashs HashTextTest------
	public String calculateHash(String x) throws NoSuchAlgorithmException {
		HashTextTest hasher = new HashTextTest("Hasher");
		return hasher.sha1(x);
	}
	
	public MapWorker_1() {
		startCache();
	}
	
	//------Main gia epikoinwnia me ton Master------
	public static void main(String[] args) throws ClassNotFoundException {
		MapWorker_1 m1 = new MapWorker_1();
		m1.openServer();
	}
	
	//------H antistoixh initialize tou skeletou------
	public void openServer() throws ClassNotFoundException {
		ServerSocket providerSocket = null;
		Socket connection = null;
		//Socket kai streams tou Reducer
		Socket requestSocketR = null;
		ObjectOutputStream outR = null;
		
		try {
				//Vazoume ton ari8mo tou port pou 8eloume gia ton Master
				providerSocket = new ServerSocket(7777);
				System.out.println("MapWorker_1 is initialized!");

				while (true) {
					//------Syndesh me ton Master------
					connection = providerSocket.accept();
					System.out.println("Successful connection.");
					ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
					ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
					
					System.out.println(in.readUTF());
					
					//dinei stoixeia ston master gia to hash
					String hash_map1;
					try {
						hash_map1 = calculateHash(IP_address+Port_number);
						out.writeUTF(hash_map1);
						out.flush();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					//------Map Procedure------
					Tuple<String,DirectionsObject> incm = (Tuple<String,DirectionsObject>) in.readObject();
					String hashincm;
					try {
						hashincm = calculateHash(incm.getKey());
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ArrayList<Tuple<String,DirectionsObject>> apotelesma = map(incm);
					//Send to Reducer
					
					//------Acknol true ston Master------
					boolean ack1 = notify_master();
					out.writeBoolean(ack1);
					out.writeUTF("MapWorker_1 is done!");
					out.flush();
					in.close();
					out.close();
					
					//------Epikoinwnia me Reducer------
					requestSocketR = new Socket(InetAddress.getByName("10.0.0.8"), 8002);
					System.out.println("Communication with Reducer has been initialized.");
					outR = new ObjectOutputStream(requestSocketR.getOutputStream());
					outR.writeUTF("Hello Reducer from MapWorker_1.");
					outR.flush();
					
					//------Stelnoume to apotelesma ston Reducer------
					sendToReducers(apotelesma);
					outR.writeObject(apotelesma);
					outR.flush();
					
					//------Telos tou MapWorker_1------
					outR.close();
					connection.close();
				}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {
			try {
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}