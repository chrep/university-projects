package master_package;

public interface Worker {
	public void openServer() throws ClassNotFoundException;
	public void waitForTasksThread();
}
