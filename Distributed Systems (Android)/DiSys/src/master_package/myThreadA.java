package master_package;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import master_package.DirectionsObject;
import master_package.MapWorker_1;
import master_package.MapWorker_2;
import master_package.MapWorker_3;
import master_package.ReduceWorker;
import master_package.myThread;

public class myThreadA extends myThread {
	
	public  myThreadA (Tuple<String,DirectionsObject> y) {
		Socket requestSocket1 = null;
		ObjectOutputStream out1 = null;
		ObjectInputStream in1 = null;
		
		try {
			requestSocket1 = new Socket(InetAddress.getByName("10.0.0.8"), 7777);
			System.out.println("Connected with MapWorker_1.");
			out1 = new ObjectOutputStream(requestSocket1.getOutputStream());
			in1 = new ObjectInputStream(requestSocket1.getInputStream());

			out1.writeUTF("Greetings from Master!");
			out1.flush();
			
			//Dokimh gia ton Mapper_1
			out1.writeObject(y);
			out1.flush();
			
			System.out.println(in1.readBoolean());
			System.out.println(in1.readUTF());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
