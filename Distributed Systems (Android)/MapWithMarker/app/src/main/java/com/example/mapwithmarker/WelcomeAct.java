package com.example.mapwithmarker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WelcomeAct extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_act);

        Button next = (Button) findViewById(R.id.button1);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //go to MapAct
                Intent myIntent = new Intent(view.getContext(), MapsMarkerActivity.class);
                startActivityForResult(myIntent, 0);

            }

        });
    }

}