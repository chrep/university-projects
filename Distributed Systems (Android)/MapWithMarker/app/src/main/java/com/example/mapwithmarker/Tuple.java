package com.example.mapwithmarker;
import java.io.Serializable;

@SuppressWarnings("hiding")
public class Tuple<String, DirectionsObject> implements Serializable {
	private static final long serialVersionUID = 1L;
	//------The union of <key, value>------
	public String key;
	public DirectionsObject value;
	public Tuple(String key, DirectionsObject value) {
		this.key = key;
		this.value = value;
	}
	public String getKey() {
		return key;
	}

	public DirectionsObject getDir() {
		return value;
	}
}