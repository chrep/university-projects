package com.example.mapwithmarker;
import java.io.Serializable;
import java.util.ArrayList;

public class DirectionsObject implements Serializable {
	private static final long serialVersionUID = 1L;
	public class java {}
	public ArrayList<String> syntetagmenes;
	public DirectionsObject() {this.syntetagmenes = null;}
	public DirectionsObject(ArrayList<String> x) {
		this.syntetagmenes = x;
	}

	public ArrayList<String> getArray() {
		return syntetagmenes;
	}
}
