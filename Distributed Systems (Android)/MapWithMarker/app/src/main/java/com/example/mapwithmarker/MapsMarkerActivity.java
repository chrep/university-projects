package com.example.mapwithmarker;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
public class MapsMarkerActivity extends AppCompatActivity
        implements OnMapReadyCallback, Serializable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    private Marker m1;
    private Marker m2;
    private ArrayList<Marker> markers = new ArrayList<>();

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        LatLng departure = new LatLng(37.971506, 23.726022);
        LatLng destination = new LatLng(37.976101, 23.725565);
        //Marker1
        m1 = googleMap.addMarker(new MarkerOptions().position(departure)
                .title("Departure Marker")
                .draggable(true));
        markers.add(m1);
        //Marker2
        m2 = googleMap.addMarker(new MarkerOptions().position(destination)
                .title("Destination Marker")
                .draggable(true));
        markers.add(m2);
        //Move Camera and zoom in
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(departure));
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {}

            @Override
            public void onMarkerDrag(Marker marker) {}

            @Override
            public void onMarkerDragEnd(Marker marker) {} });

        //Dokimes dhmiourgias Polyline
    /*    ArrayList<LatLng> test = new ArrayList<>();
        test.add(new LatLng(37.971063, 23.723729));
        test.add(new LatLng(37.971173, 23.723643));
        test.add(new LatLng(37.971629, 23.723740));
        test.add(new LatLng(37.971865, 23.723354));
        test.add(new LatLng(37.971738, 23.723564));
        test.add(new LatLng(37.971976, 23.723461));
        PolylineOptions polyline1 = new PolylineOptions().clickable(false).addAll(test);
        googleMap.addPolyline(polyline1);

           PolylineOptions polyline1 = new PolylineOptions().clickable(false)
                .add(
                (departure),
                new LatLng(37.971063, 23.723729),
                new LatLng(37.971173, 23.723643),
                new LatLng(37.971629, 23.723740),
                new LatLng(37.971865, 23.723354),
                new LatLng(37.971976, 23.723461),
                new LatLng(37.972213, 23.723761),
                new LatLng(37.972508, 23.724423),
                new LatLng(37.972757, 23.725213),
                new LatLng(37.973295, 23.725210),
                new LatLng(37.973340, 23.725084),
                new LatLng(37.973552, 23.725192),
                new LatLng(37.974110, 23.725320),
                new LatLng(37.974567, 23.725235),
                new LatLng(37.975311, 23.725503),
                new LatLng(37.975993, 23.725833),
                new LatLng(37.976069, 23.725651));
        googleMap.addPolyline(polyline1); */


        Button search = (Button) findViewById(R.id.button2);
        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                LatLng source = null;
                LatLng dest = null;

                for(int i=0;i<2;i++){
                    if(i == 0) {
                        source =  markers.get(i).getPosition();
                    }
                    else {
                        dest = markers.get(i).getPosition();
                    }
                }
                testCLient test = new testCLient();
                test.execute(markers);

                PolylineOptions polyline = new PolylineOptions().clickable(false).add(source,dest);
                googleMap.addPolyline(polyline);

            }
        });


    }

    private class testCLient extends AsyncTask<Object, Object, ArrayList<LatLng>> {

        @Override
        protected ArrayList<LatLng> doInBackground(Object... params) {
            publishProgress("Loading...");

            //Socket kai streams tou Master
            Socket requestSocketMaster = null;
            ObjectOutputStream outm = null;
            ObjectInputStream inm = null;
            String message = "Map-Reduce procedure is complete.";
            ArrayList<LatLng> results = null;
            try {
                //Epikoinwnia me Master
                requestSocketMaster = new Socket(InetAddress.getByName("10.0.0.8"), 8000);
                System.out.println("Client-Master communication has been initialized.");
                outm = new ObjectOutputStream(requestSocketMaster.getOutputStream());
                inm = new ObjectInputStream(requestSocketMaster.getInputStream());


                //------To tuple pou dinei o client ston master------
                DirectionsObject t1 = new DirectionsObject();
                String key1 = String.valueOf(markers.get(0).getPosition().latitude) + "," + String.valueOf(markers.get(0).getPosition().longitude);
                String key2 = String.valueOf(markers.get(1).getPosition().latitude) + "," + String.valueOf(markers.get(1).getPosition().longitude);
                String key = key1 + "," + key2;
                Tuple<String, DirectionsObject> x = new Tuple<>(key, t1);

                try {
                    outm.writeObject(x);
                }
                catch(IOException ex) {
                    ex.printStackTrace();
                }

                try {
                    Tuple<String, DirectionsObject> result = (Tuple<String, DirectionsObject>) inm.readObject();
                    DirectionsObject dirOb = result.getDir();

                    System.out.println("Got'em");
                    System.out.println(inm.readUTF());
                    System.out.println("Server> " + message);

                    inm.close();

                    requestSocketMaster.close();

                    ArrayList<String> j = dirOb.getArray();
                    ArrayList<LatLng> tolatlng = new ArrayList<>();
                    for(int i=0; i<j.size(); i++) {
                        String[] test =  j.get(i).split(",");
                        double latitude = Double.parseDouble(test[0]);
                        double longitude = Double.parseDouble(test[1]);
                        LatLng newlatlng = new LatLng(latitude,longitude);
                        tolatlng.add(newlatlng);
                    }

                    results = tolatlng;


                } catch (ClassNotFoundException exc) {
                    System.out.println("Error reading!");
                }


            } catch (UnknownHostException unknownHost) {
                System.err.println("You are trying to connect to an unknown host!");
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

            try {
                outm.flush();
                outm.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return results;
        }

        @Override
        protected void onPreExecute(){}

        @Override
        protected void onPostExecute (ArrayList<LatLng> results){
            if (results == null) {
                System.out.println("Error finding route.");
            }
            else {
                System.out.println("Successful");
            }

        }

        protected void onProgressUpdate (String progress){}

    }
}


